export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  REGION: "ap-southeast-1",
  s3: {
    BUCKET: "rentraffic-uploadfiles"
  },
  apiGateway: {
    REGION: "ap-southeast-1",
    URL: "https://7f27z7g4hk.execute-api.ap-southeast-1.amazonaws.com/prod"
  },
  cognito: {
    USER_POOL_ID : 'ap-southeast-1_wuGvB7xwa',
    APP_CLIENT_ID : 'l6gfl41acch1jftnccjrlofq8',
    REGION: 'ap-southeast-1',
    IDENTITY_POOL_ID: 'ap-southeast-1:6830beb2-65ef-45a7-8dcd-436ee2a4a416',
  }
};
