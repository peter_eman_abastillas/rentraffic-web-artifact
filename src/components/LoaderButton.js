import React from "react";
import "./LoaderButton.css";
import Button from 'material-ui/Button';
import Loop from 'material-ui-icons/Loop'


export default ({
  isLoading,
  text,
  loadingText,
  className = "",
  disabled = false,
  ...props
}) =>
  <Button
    className={`LoaderButton ${className}`}
    disabled={disabled || isLoading}
    {...props}
    raised color="primary"
    fullWidth
  >
    {isLoading && <Loop className="spinning" />}
    {!isLoading ? text : loadingText}
  </Button>;
