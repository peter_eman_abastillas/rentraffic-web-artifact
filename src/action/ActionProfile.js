import AWS from "aws-sdk";
import {authUser} from '../libs/awsLib'
import config from "../config";
import Validate from '../libs/Validate'
var defaults_payload = [
  "first_name",
  "last_name",
  "phone_home",
  "phone_work",
  "phone_mobile",
  "profile_pic",
  "location",
  "address",
  "city",
  "lat",
  "lng"
];


export function getUser(userToken, username) {
  return async function (dispatch) {
    const url = `${config.apiGateway.URL}/profile/${username}`;

    dispatch({type:"FETCH_USER_LOADING", isLoaded: false})
    fetch(url, {
      method: 'GET',
      headers:{
        "Authorization": userToken,
      },

    }).then((resp)=>{
        return resp.json()
    })
    .then((resp) => {
        dispatch({type:"FETCH_USER_LOADED", isLoaded: true, payload: resp})
    })
    .catch((err)=> {
        dispatch({type:"FETCH_USER_ERROR", isLoaded: true, error: err})
    });

  }

}


export function s3Upload(file, i) {
  return async function (dispatch) {
    const key = new Date().getTime();
    dispatch({type: `PROFILE_IMAGE_UPLOADING`, image_temp: file, key:key})

    var upload = await _s3Upload(file);
    console.debug(upload);
    dispatch({type: `PROFILE_IMAGE_UPLOADED`, image: upload.Location, key:key})
  }
}
export async function _s3Upload(file) {
  if (!await authUser()) {
    throw new Error("User is not logged in");
  }

  const s3 = new AWS.S3({
    params: {
      region: config.REGION,
      Bucket: config.s3.BUCKET
    }
  });
  const filename = `${AWS.config.credentials
    .identityId}/${Date.now()}-${file.name}`;
  return s3
    .upload({
      Key: filename,
      Body: file,
      ContentType: file.type,
      ACL: "public-read"
    })
    .promise();

}
export function handleItemEntry(name, value) {
  return async function (dispatch) {
    dispatch({type: `PROFILE_EDIT_${name.toUpperCase()}`, [name]: value})
  }
}

export function saveTheUser(payload, user_token) {
  var payload_data={};
  defaults_payload.forEach((i)=>{
    switch(i=='profile_pic') {
      case'profile_pic':
        payload_data[i] = JSON.stringify(payload[i]);
        break
      default:
        payload_data[i] = payload[i];
    }
  });
  Object.keys(payload_data).forEach((key) => (payload_data[key] == null) && delete payload_data[key]);

  return async function (dispatch) {
    dispatch({type: `PROFILE_SAVING_BEGIN`, status: 0});

    fetch(
      config.apiGateway.URL + "/profile/update",
      {
        method: 'POST',
        headers: {
          "Authorization": user_token,
        },
        body: JSON.stringify(payload_data)
      })
      .then((resp)=>{
        return resp.json()
      })
      .then((res) => {
        dispatch({type: `PROFILE_SAVING_SUCCESS`, status: 1});

      }).catch(() => {
        dispatch({type: `PROFILE_SAVING_ERROR`, status: 3});

    })
  }
}