import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';
import { invokeApig } from '../libs/awsLib';
import "./Home.css";
import {connect} from "react-redux"
import GetItems from "./Rent/GetItems";


class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      notes: []
    };
  }

  notes() {
    return invokeApig({ path: "/notes" });
  }

  handleNoteClick = event => {
    event.preventDefault();
    this.props.history.push(event.currentTarget.getAttribute("href"));
  }



  renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple note taking app</p>
        <div>
          <Link to="/login" className="btn btn-info btn-lg">
            Login
          </Link>
          <Link to="/signup" className="btn btn-success btn-lg">
            Signup
          </Link>
        </div>
      </div>
    );
  }

  renderNotes() {
    return (<GetItems/>);
  }

  render() {
    return (
      <div className="Home">
        {this.props.isAuthenticated ? this.renderNotes() : this.renderLander()}
      </div>
    );
  }
}
Home = connect((store) => {
  return {...store}
})(Home);


export default withRouter(Home);
