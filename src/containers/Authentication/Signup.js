import React, { Component } from "react";

import {
  AuthenticationDetails,
  CognitoUserPool
} from "amazon-cognito-identity-js";
import LoaderButton from "../../components/LoaderButton";
import config from "../../config";
import "./Signup.css";
import Facebook from "./Facebook"
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import { Link } from "react-router-dom";

const styles = theme => ({

});



class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: "",
      confirmPassword: "",
      confirmationCode: "",
      newUser: null
    };
  }

  validateForm() {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const newUser = await this.signup(this.state.email, this.state.password);
      this.setState({
        newUser: newUser
      });
    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.confirm(this.state.newUser, this.state.confirmationCode);
      await this.authenticate(
        this.state.newUser,
        this.state.email,
        this.state.password
      );

      this.props.userHasAuthenticated(true);
      this.props.history.push("/");
    } catch (e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  signup(email, password) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    });

    return new Promise((resolve, reject) =>
      userPool.signUp(email, password, [], null, (err, result) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(result.user);
      })
    );
  }

  confirm(user, confirmationCode) {
    return new Promise((resolve, reject) =>
      user.confirmRegistration(confirmationCode, true, function(err, result) {
        if (err) {
          reject(err);
          return;
        }
        resolve(result);
      })
    );
  }

  authenticate(user, email, password) {
    const authenticationData = {
      Username: email,
      Password: password
    };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) =>
      user.authenticateUser(authenticationDetails, {
        onSuccess: result => resolve(),
        onFailure: err => reject(err)
      })
    );
  }

  renderConfirmationForm(classes) {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <TextField
          id="confirmationCode"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={this.handleChange('confirmationCode')}
          label="Code"
          className={classes.textField}
          value={this.state.confirmationCode}
          margin="normal"
          fullWidth
        />

        <LoaderButton

          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Sign up"
          loadingText="Signing up…"
        />
      </form>
    );
  }

  renderForm(classes) {
    return (
        <form onSubmit={this.handleSubmit}>
          <Facebook/>
          <TextField
            id="Username"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={this.handleChange('email')}
            label="Username/Email"
            className={classes.textField}
            value={this.state.email}
            margin="normal"
            fullWidth
          />
          <TextField
            id="password"
            InputLabelProps={{
              shrink: true,
            }}
            type="password"
            onChange={this.handleChange('password')}
            label="Password"
            className={classes.textField}
            value={this.state.password}
            margin="normal"
            fullWidth
          />
          <TextField
            id="confirmPassword"
            InputLabelProps={{
              shrink: true,
            }}
            type="password"
            onChange={this.handleChange('confirmPassword')}
            label="Confirm Password"
            className={classes.textField}
            value={this.state.confirmPassword}
            margin="normal"
            fullWidth
          />

          <LoaderButton
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Verify"
            loadingText="Verifying code..."
          />
        </form>
    );
  }

  render() {
    const {classes} = this.props;

    return (
      <Paper elevation={2} className="Login">
        {this.state.newUser === null
          ? this.renderForm(classes)
          : this.renderConfirmationForm(classes)}
      </Paper>
    );
  }
}



Signup.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Signup);