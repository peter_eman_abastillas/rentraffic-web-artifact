import React, { Component } from "react";
import {
  CognitoUserPool,
  AuthenticationDetails,
  CognitoUser
} from "amazon-cognito-identity-js";
import LoaderButton from "../../components/LoaderButton";
import config from "../../config";
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import { Link } from "react-router-dom";

import "./Login.css";
const styles = theme => ({

});
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }
//https://rentraffic.auth.ap-southeast-1.amazoncognito.com/oauth2/authorize?response_type=token&client_id=l6gfl41acch1jftnccjrlofq8&redirect_uri=https://www.amazon.com
  login(email, password) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    });
    const user = new CognitoUser({ Username: email, Pool: userPool });
    const authenticationData = { Username: email, Password: password };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) =>
      user.authenticateUser(authenticationDetails, {
        onSuccess: result => resolve(),
        onFailure: err => reject(err)
      })
    );
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.login(this.state.email, this.state.password);

      this.props.userHasAuthenticated(true);
    } catch (e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  render() {
    const {classes} = this.props
    return (
      <Paper elevation={2} className="Login">
        <form onSubmit={this.handleSubmit}>
          <TextField
            id="Username"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={this.handleChange('email')}
            label="Username/Email"
            className={classes.textField}
            value={this.state.email}
            margin="normal"
            fullWidth
          />
          <TextField
            id="Username"
            InputLabelProps={{
              shrink: true,
            }}
            type="password"
            onChange={this.handleChange('password')}
            label="Password"
            className={classes.textField}
            value={this.state.password}
            margin="normal"
            fullWidth
          />


          <Link to="/forgot_password">Forgot password</Link>

          <LoaderButton

            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Logging in…"
          />
        </form>
      </Paper>
    );
  }
}



Login.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Login);