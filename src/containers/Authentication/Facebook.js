import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {connect} from "react-redux"
import FacebookLogin from 'react-facebook-login';
import {facebookOAuth} from "../../libs/awsLib"

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flex:1
  },

});


class Facebook extends React.Component {

  responseFacebook(data) {
    facebookOAuth(data);
    // console.log(arguments);
  }
  render() {
    const {classes, } = this.props;
    return (
      <FacebookLogin
        appId="567485946977277"
        autoLoad={true}
        version="2.5"
        fields="name,email,picture"
        onClick={()=>console.debug(arguments)}
        callback={(data)=>this.responseFacebook(data)} />
    )
  }
}

Facebook.propTypes = {
  classes: PropTypes.object.isRequired,
};

Facebook = connect((store) => {
  return {...store}
})(Facebook);

export default withStyles(styles)(Facebook);






