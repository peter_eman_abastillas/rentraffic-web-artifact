import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import {connect} from "react-redux"
import { withRouter, Link } from 'react-router-dom';
import Cards from './Items/Cards'
const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 180,
    margin:"0 auto 0",
    top: 10,
    height: 180,
  },
};
class GetItems extends React.Component {
  state = {
    items:[]
  };
  async componentWillMount() {
    await fetch(
      `https://7f27z7g4hk.execute-api.ap-southeast-1.amazonaws.com/prod/rent/get`,
      {
        method: 'GET',
      }
      ).then((resp)=>resp.json())
      .then((response)=>{
        console.debug(response)
        this.setState({
          items:response
        })
      });
  }
  render() {
    const { classes, theme } = this.props;
    console.debug(this.props);

    return (
      <Grid container spacing={24}>
        {(!this.state.items.length) ? null :
          this.state.items.map((card) => {
            console.debug(card);
            return (
              <Grid item>
                <Cards card={card}/>
              </Grid>
            )
          })
        }
      </Grid>
    );
  }
}

GetItems.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
GetItems = connect((store) => {
  return {...store}
})(GetItems);

export default withRouter(withStyles(styles, { withTheme: true })(GetItems));