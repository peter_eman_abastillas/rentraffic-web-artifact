import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import {handleItemEntry} from "../../../action/ActionCreateRentForm";
import {connect} from "react-redux"
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flex:1
  },
  autoComplete: {
    flex:1
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
});


class ItemPriceDetails extends React.Component {
  state = {
    per:"hour",
    amount:0,
    availability:1,
    deposit:0
  }
  handleChange = name => event => {
    this.props.dispatch(handleItemEntry(name, event.target.value));
    this.setState({
      [name]: event.target.value,
    })
  }
  render() {
    const {classes, CreateRentForm} = this.props;
    return (
        <form className={classes.container} noValidate autoComplete="off">
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="amount">Amount</InputLabel>
                <Input
                  id="adornment-amount"
                  value={CreateRentForm.amount}
                  error={(CreateRentForm.validation.indexOf("amount")!==-1)}

                  onChange={this.handleChange('amount')}
                  margin="dense"
                  type="number"
                  startAdornment={<InputAdornment position="start">$</InputAdornment>}
                />
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="age-simple">Per</InputLabel>
                <Select
                  value={CreateRentForm.per}
                  error={(CreateRentForm.validation.indexOf("per")!==-1)}
                  onChange={this.handleChange('per')}
                  inputProps={{
                    name: 'per',
                    id: 'per-simple',
                  }}
                  margin="dense"
                  startAdornment={<InputAdornment position="start">/</InputAdornment>}
                >
                  <MenuItem value="hour">
                    <em>hour</em>
                  </MenuItem>
                  <MenuItem value="day">day</MenuItem>
                  <MenuItem value="week">week</MenuItem>
                  <MenuItem value="month">month</MenuItem>
                  <MenuItem value="none">none</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="deposit">Deposit/Assurance</InputLabel>
                <Input
                  error={(CreateRentForm.validation.indexOf("deposit")!==-1)}

                  id="adornment-deposit"
                  value={CreateRentForm.deposit}
                  onChange={this.handleChange('deposit')}
                  type="number"
                  margin="dense"
                  startAdornment={<InputAdornment position="start">$</InputAdornment>}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="availability">Available Items</InputLabel>
                <Input
                  error={(CreateRentForm.validation.indexOf("availability")!==-1)}

                  id="adornment-availability"
                  value={CreateRentForm.availability}
                  type="number"
                  onChange={this.handleChange('availability')}
                  margin="dense"
                />
              </FormControl>
            </Grid>
          </Grid>
        </form>
      )
    }
}

ItemPriceDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

ItemPriceDetails = connect((store) => {
  return {...store}
})(ItemPriceDetails);

export default withStyles(styles)(ItemPriceDetails);