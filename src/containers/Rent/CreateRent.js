import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MobileStepper from 'material-ui/MobileStepper';
import Paper from 'material-ui/Paper';
import {connect} from "react-redux"
import ItemDetails from './Forms/ItemDetails'
import ItemPriceDetails from './Forms/ItemPriceDetails'
import ItemDescription from './Forms/ItemDescription'
import ItemImages from './Forms/ItemImages'
import {handleSubmit} from "../../action/ActionCreateRentForm";
import Button from 'material-ui/Button';
import KeyboardArrowLeft from 'material-ui-icons/KeyboardArrowLeft';
import KeyboardArrowRight from 'material-ui-icons/KeyboardArrowRight';
import Avatar from 'material-ui/Avatar';


const styles = {
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  heads: {
    flexDirection:"row",
    marginBottom:10,
    display:"flex"
  },
  progressBar: {
    width:'100%'
  },
  containerMain: {
    padding:20,
    margin:20,
    flexDirection:"row",
    paddingBottom:60,
    minHeight: 300,
    position: 'relative'
  },
  stepper: {
    top: -10,
    backgroundColor: 'transparent',
    position: 'relative',
    margin: -10
  },
  actionButton: {
    position:'absolute',
    bottom:10,
    flex:1,
    right:10,
    textAlign:"right",
  }

};

class CreateRent extends React.Component {
  state = {
    activeStep: 0,
  };
  steps = [
    <ItemDetails {...this.props}/>,
    <ItemDescription {...this.props}/>,
    <ItemPriceDetails {...this.props}/>,
    <ItemImages {...this.props}/>,

  ];
  header = [
    "Items Details", "Items Description", "Price Details", "Upload item images", "Confirm and Submit"
  ]
  handleNext = () => {
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  };

  handleBack = () => {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  };

  handleSubmit = () => {
    const { dispatch, CreateRentForm } = this.props;

    dispatch(handleSubmit(CreateRentForm, this.props.token));

  };

  render() {
    const { classes, theme } = this.props;
    const MAX = this.steps.length;
    return (
      <div style={{padding:"60px 20px"}}>
        <Paper className={classes.containerMain+ " clearfix"}>
          <div className={classes.heads}>
            <Avatar>{this.state.activeStep+1}</Avatar>
            <h1 style={{flex: 2, marginTop: 0,marginLeft: 10}}>{this.header[this.state.activeStep]}</h1>
          </div>
          <MobileStepper
            className={classes.stepper}
            type="progress"
            steps={MAX}
            position="static"
            classes={{
              progress: classes.progressBar
            }}
            activeStep={this.state.activeStep}
          />
          <div style={{    display: "flex"}}>
            {this.steps[this.state.activeStep]}
            <Paper style={{flex: 1, height:200, margin: 10}}></Paper>
          </div>
          <div className={classes.actionButton}>
            <Button dense onClick={this.handleBack} disabled={this.state.activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              Back
            </Button>
            <Button dense onClick={this.state.activeStep < MAX ? this.handleNext : this.handleSubmit}>
              {this.state.activeStep < MAX ? "Next" : "Submit"}
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          </div>
        </Paper>
      </div>
    );
  }
}

CreateRent.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
CreateRent = connect((store) => {
  return {...store}
})(CreateRent);

export default withStyles(styles, { withTheme: true })(CreateRent);