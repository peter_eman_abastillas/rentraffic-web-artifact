import React, { Component } from "react";
import {connect} from "react-redux"
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Grid from 'material-ui/Grid';
import Avatar from 'material-ui/Avatar';
import classNames from 'classnames';
import FileUpload from 'material-ui-icons/FileUpload';
import Button from 'material-ui/Button';
import {s3Upload, saveTheUser, handleItemEntry} from "../../../action/ActionProfile";
import config from "../../../config";
import Dropzone from 'react-dropzone'
import EditContacts from './EditContacts';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin:"0 auto"
  },
  card:{},
  media:{},
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  avatar: {
    margin: 10,
  },
  button: {
    margin: theme.spacing.unit,
  },
  bigAvatar: {
    width: 180,
    margin:"0 auto 0",
    top: 10,
    height: 180,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  uploadBox: {
    border: "1 dashed",
    textAlign: "center"
  },
});

/**
 * id,
 * last_login,
 * active,
 * email,
 * first_name,
 * last_name,
 * phone,
 * profile_pic,
 * user_type,
 * location,
 * address,
 * city,
 * lat,
 * lng
 */
class PersonalInfo extends Component {
  state={
    first_name: "peter",
  }
  onDrop = (files) => {
    files.forEach((v, i)=> {
      console.debug(v);
      this.uploadFile(v, i)
    });
  }
  handleSubmit = name => event => {
    this.props.dispatch(saveTheUser(this.props.UserProfile, this.props.token))

  };
  handleChange = name => event => {
    this.props.dispatch(handleItemEntry(name, event.target.value));

  };
  uploadFile = async (file, i) =>  {
    if (file && file.size > config.MAX_ATTACHMENT_SIZE) {
      alert('Please pick a file smaller than 5MB');
      return;
    }

    // this.setState({ isUploading: this.images.length });
    try {
      this.props.dispatch(s3Upload(file, i, this.props.token));
    }
    catch(e) {
      alert(e);
      // this.setState({ isUploading: false });
    }
  }

  render() {
    console.debug(this.props);
    const { classes, UserProfile } = this.props;
    let profile_pic = <Avatar
      alt="Adelle Charles"
      className={classNames(classes.avatar, classes.bigAvatar)}
    >No Profile</Avatar>;

    if(!!UserProfile.profile_pic) {
      profile_pic = <Avatar
        alt="Adelle Charles"
        src={UserProfile.profile_pic[0]}
        className={classNames(classes.avatar, classes.bigAvatar)}
      />
    }

    return (
      <div className="row">
        <div className="row"  style={{marginBottom:20}}>
          <div className="col-sm-3 text-center">
            <Dropzone
              onDrop={this.onDrop}
              className={classes.uploadBox}
              accept="image/*, video/*"
            >
            {profile_pic}
            <Button className={classes.button} raised color="primary">
              <FileUpload className={classes.leftIcon} />
              Upload
            </Button>
          </Dropzone>
          </div>
          <div className="col-sm-7">
            <h2>Profile</h2>

            <TextField
            id="first_name"
            label="First Name"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={this.handleChange('first_name')}
            className={classes.textField}
            value={UserProfile.first_name}
            margin="normal"
            fullWidth
          />
          <TextField
            id="last_name"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={this.handleChange('last_name')}
            label="Last Name"
            className={classes.textField}
            value={UserProfile.last_name}
            margin="normal"
            fullWidth
          />
          <TextField
            id="Email"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={this.handleChange('email')}
            label="Email"
            className={classes.textField}
            value={UserProfile.email}
            margin="normal"
            disabled
            fullWidth
          />
            <h2>Contacts and Address</h2>
          <EditContacts {...this.props}/>

          </div>
      </div>
      <Divider/>
        <div>
          <Button className={classes.button} onClick={this.handleSubmit()} raised color="primary">Save</Button>
        </div>
    </div>
    )
  }
}


PersonalInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

PersonalInfo = connect((store) => {
  return {...store}
})(PersonalInfo);

export default withStyles(styles, { withTheme: true })(PersonalInfo);