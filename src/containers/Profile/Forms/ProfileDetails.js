import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography';
import SwipeableViews from 'react-swipeable-views';
import EditContacts from './EditContacts';
import PersonalInfo from './PersonalInfo';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin:"0 auto"
  },
  card:{},
  media:{},
  avatar:{},
  bigAvatar:{},
});
function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
class ProfileDetails extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <div className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Personal Info" />
          <Tab label="Credentials" />
          <Tab label="Payment Method" />
        </Tabs>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer dir={theme.direction}>
            <PersonalInfo {...this.props}/>
          </TabContainer>
          <TabContainer dir={theme.direction}>
          </TabContainer>
          <TabContainer dir={theme.direction}>asdsa</TabContainer>
        </SwipeableViews>
      </div>
    );
  }
}

ProfileDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ProfileDetails);