import React, { Component } from "react";
import {connect} from "react-redux"
import { withStyles } from 'material-ui/styles';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import Input,{ InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import {s3Upload, saveTheUser, handleItemEntry} from "../../../action/ActionProfile";
import InputGeocode from '../../../libs/geocode/InputGeocode';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin:"0 auto"
  },
  card:{},
  media:{},
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  input: {
    margin: theme.spacing.unit,
  },
  avatar:{},
  bigAvatar:{},
  formControl: {
    width:"100%"
  }

});

function TextMaskCustom(props, type) {
  const { inputRef, ...other } = props;
  const format = {
    "landline" : ['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    "mobile" : ['+', /^(\s*|\d+)$/,/^(\s*|\d+)$/,'-', /^(\s*|\d+)$/, /\d/, /\d/, '-',  /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
  }
  return (
    <MaskedInput
      {...other}
      ref={inputRef}
      mask={format['mobile']}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

TextMaskCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      ref={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

class EditContacts extends React.Component {
  state = {
    textmask: '(0  )    -    ',
  };

  handleChange = name => event => {
    this.props.dispatch(handleItemEntry(name, event.target.value));
  };

  render() {
    const { classes, UserProfile } = this.props;
    return (
      <div className={classes.container}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink={true}  htmlFor="mobile">Mobile phone number</InputLabel>
            <Input
              id="adornment-phone-mobile"
              value={UserProfile.phone_mobile}
              inputComponent={TextMaskCustom}
              inputlabelprops={{
                shrink: true,
              }}
              fullWidth
              onChange={this.handleChange('phone_mobile')}
              className={classes.input}
              inputProps={{
                'aria-label': 'Description',
              }}
            />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel shrink={true}  htmlFor="">Home phone number</InputLabel>
            <Input
              id="adornment-phone-home"
              value={UserProfile.phone_home}
              inputlabelprops={{
                shrink: true,
              }}
              fullWidth
              inputComponent={TextMaskCustom}
              onChange={this.handleChange('phone_home')}
              className={classes.input}
              inputProps={{
                'aria-label': 'Description',
              }}
            />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel shrink={true} htmlFor="work">Work phone number</InputLabel>
            <Input
              id="adornment-phone-work"
              inputlabelprops={{
                shrink: true,
              }}
              fullWidth
              value={UserProfile.phone_work}
              inputComponent={TextMaskCustom}
              onChange={this.handleChange('phone_work')}
              className={classes.input}
              inputProps={{
                'aria-label': 'Description',
              }}
            />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel shrink={true} htmlFor="address">Address</InputLabel>
          <Input
            id="adornment-amount"
            value={UserProfile.address || undefined}
            //error={(UserProfile.validation.indexOf("address")!==-1)}
            onChange={this.handleChange('address')}
            margin="dense"
            fullWidth
          />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputGeocode
            inputLabel={<InputLabel shrink={true} htmlFor="address">City</InputLabel>}
            inputProps={{fullWidth:true, }}
            getSelectedLocation={(option)=>console.debug(option)}
          />

        </FormControl>
      </div>
    );
  }
}


EditContacts.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

EditContacts = connect((store) => {
  return {...store}
})(EditContacts);

export default withStyles(styles, { withTheme: true })(EditContacts);