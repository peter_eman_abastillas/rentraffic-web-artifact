import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import LocationCity from 'material-ui-icons/LocationCity';
import LocationOn from 'material-ui-icons/LocationOn';
import Email from 'material-ui-icons/Email';
import Avatar from 'material-ui/Avatar';
import classNames from 'classnames';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 180,
    margin:"0 auto 0",
    top: 10,
    height: 180,
  },
};

class User extends Component {
  handleLink = (event, arg) => {
    this.props.history.push(arg);
  }

  render() {
    const {classes, profile} = this.props;
    let profile_pic = <Avatar
      alt= {profile.first_name + " " + profile.last_name}
      className={classNames(classes.avatar, classes.bigAvatar)}
    >{profile.first_name.charAt(0).toUpperCase()}{profile.last_name.charAt(0).toUpperCase()}</Avatar>;

    if(!!profile.profile_pic) {
      profile_pic = <Avatar
        alt= {profile.first_name + " " + profile.last_name}
        src={profile.profile_pic[0]}
        className={classNames(classes.avatar, classes.bigAvatar)}
      />
    }
    return (
      <div>
        <Card className={classes.card}>
          <CardMedia
            className={classes.media}
            image="/img/bg-profile.jpg"
          >
            {profile_pic}
          </CardMedia>
          <CardContent>
            <Typography type="headline" component="h2">
              {profile.first_name} {profile.last_name}
            </Typography>
          </CardContent>
          <CardActions>
            <Button onClick={(e)=>this.handleLink(e, '/user/edit')} size="small" color="primary">
              Edit
            </Button>
            <Button size="small" color="primary">
              Rate
            </Button>
          </CardActions>
        </Card>
        <List component="nav">
          <ListItem button>
            <ListItemIcon>
              <LocationCity />
            </ListItemIcon>
            <ListItemText primary="City" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <LocationOn />
            </ListItemIcon>
            <ListItemText primary="Location" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <Email />
            </ListItemIcon>
            <ListItemText primary={profile.email} />
          </ListItem>
        </List>
        <Divider />
        <List component="nav">
          <ListItem button>
            <ListItemText primary="Trash" />
          </ListItem>
          <ListItem button component="a" href="#simple-list">
            <ListItemText primary="Spam" />
          </ListItem>
        </List>
      </div>
    );
  }
}

User.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(User);