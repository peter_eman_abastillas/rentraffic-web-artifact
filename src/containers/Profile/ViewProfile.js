import React, { Component } from "react";
import {connect} from "react-redux"
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import config from "../../config";
import User from './Containers/User';
import ProfileDetails from './Containers/ProfileDetails';
import { withRouter, Link } from 'react-router-dom';
import Grid from 'material-ui/Grid';

const styles = {
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  heads: {
    flexDirection:"row",
    marginBottom:10,
    display:"flex"
  },
  progressBar: {
    width:'100%'
  },
  containerMain: {
    padding:20,
    margin:20,
    flexDirection:"row",
    paddingBottom:60,
    minHeight: 300,
    position: 'relative'
  },
  stepper: {
    top: -10,
    backgroundColor: 'transparent',
    position: 'relative',
    margin: -10
  },
  actionButton: {
    position:'absolute',
    bottom:10,
    flex:1,
    right:10,
    textAlign:"right",
  }
};

class ViewProfile extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {UserProfile} = this.props
    console.debug();
    return (
      <Grid container spacing={24}>
        <Grid item sm={12} md={3}>
          <User {...this.props} profile={UserProfile}/>
        </Grid>
        <Grid item sm={12} md={9}>
          <ProfileDetails profile={UserProfile}/>
        </Grid>
      </Grid>
    );
  }
}


ViewProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
ViewProfile = connect((store) => {
  return {...store}
})(ViewProfile);

export default withRouter(withStyles(styles, { withTheme: true })(ViewProfile));