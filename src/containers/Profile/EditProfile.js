import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import {connect} from "react-redux"
import { withRouter, Link } from 'react-router-dom';
import ProfileDetails from './Forms/ProfileDetails'

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 180,
    margin:"0 auto 0",
    top: 10,
    height: 180,
  },
};
class EditProfile extends React.Component {


  render() {
    const { classes, theme } = this.props;
    console.debug(this.props);

    return (
      <Grid container spacing={24}>
          <ProfileDetails {...this.props}/>
      </Grid>
    );
  }
}

EditProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
EditProfile = connect((store) => {
  return {...store}
})(EditProfile);

export default withRouter(withStyles(styles, { withTheme: true })(EditProfile));