export default class Validate {
  hasError = false;
  errorType=[];
  errorMessage = [];
  email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  constructor(key, item) {
    this.item = item;
    this.key = key;

    return this;
  }
  validate(valid) {
    this.hasError = !valid;
    if (!valid) {
      if(this.errorMessage.indexOf(this.key)===-1) this.errorMessage.push(this.key);
    }
  }
  isNumber() {
    let valid = !isNaN(this.item);
    this.validate(valid);
    if(!valid) this.errorType.push("not a number");
    return this;
  }

  required() {
    let valid = !!this.item;
    switch (typeof this.item) {
      case "object" :
        valid = (Object.keys(this.item).length);
        break;
      case "array" :
        valid = this.item.length;
        break;
      default:
        valid = !!this.item;
        break;
    }
    this.validate(valid);
    if(!valid) this.errorType.push("is Required");
    return this;
  }
  isEmail() {
    let valid = this.email.test(this.item);
    this.validate(valid);
    if(!valid) this.errorType.push("Not an email");

    return this;
  }
  max(i) {
    let valid = (!!this.item && this.item.length<i);
    this.validate(valid);
    if(!valid) this.errorType.push("Not an email");

    return this;
  }
  min(i) {
    let valid = (this.item.length>i);
    this.validate(valid);
    if(!valid) this.errorType.push("Not an email");

    return this;
  }
  isPhoneNumber() {
    let valid = this.phone.test(this.item);
    this.validate(valid);
    if(!valid) this.errorType.push("Not phone number");
    return this;
  }
}
