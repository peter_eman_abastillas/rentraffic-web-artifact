import React, {Component} from  "react";
import Input from "material-ui/Input";
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Menu, { MenuItem,MenuList } from 'material-ui/Menu';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import './style.css'
const styles = theme => {
  return {
    menu: {
      position:"absoulute",
      maxWidth:200
    }
  }
};

class InputGeocode extends Component {
  inputGeo={};
  state={
    anchorEl: null,
    places: [],
    placeValue:"",
    selected: null
  };
  google;
  places=[];
  service;
  place="";

  getSuggestion = event => {
    let _this = this;
    let value = event.target.value;
    this.service.getQueryPredictions(
      { input: value},
      (predictions, status)=>{
        console.debug(predictions, status)
        if (status !== _this.google.maps.places.PlacesServiceStatus.OK) {
          alert(status);
          return;
        }

        console.debug(predictions);
        _this.setState({
          places:predictions,
          placeValue: value,
          anchorEl:this.inputGeo,
        }, ()=>_this.places=predictions);
      }
    );

  }
  initMap() {
    if(typeof window.google === "undefined") return;
    let _this = this;

    let input = this.inputGeo;//document.getElementById('pac-input');

    let autocomplete = new this.google.maps.places.Autocomplete(input);
    this.service = new this.google.maps.places.AutocompleteService(input);

    autocomplete.addListener('place_changed', function() {
      let place = autocomplete.getPlace();
      if (place.address_components) {
        _this.place = place.address_components;
      }
    });

    input.removeEventListener('keydown', ()=>{});
    input.addEventListener('keydown', function(ev) {
      if (ev.key === "ArrowDown")  {
        let index = _this.getChildIndex(document.getElementsByClassName('pac-item'), true);
        _this.setState({
          selected : index,
          placeValue: _this.state.places[index].description
        });
      } else if (ev.key === "ArrowUp") {
        let index = _this.getChildIndex(document.getElementsByClassName('pac-item'), false);
        _this.setState({
          selected : index,
          placeValue: _this.state.places[index].description
        });
      }
    });
  }
  getChildIndex = function(child, directionDown){
    if(!child.length) return;
    let selected = document.getElementsByClassName('pac-item-selected')[0];
    let parent = document.getElementsByClassName('pac-container')[0];
    if(!selected) {
      return (directionDown) ? 0 : parent.children.length-1;
    }
    if(!parent.children.length) return;

    if(directionDown) {
      let children = parent.children;
      let i = 0;

      for (; i < children.length; i++) {
        if (selected === children[i]) {
          break;
        }
      }
      return i + 1;
    } else {
      let children = parent.children;
      let i = parent.children.length-1
      console.debug(i);

      for (; i > 0; i--) {
        if (selected === children[i]) {
          break;
        }
      }
      return i-1;
    }
  };
  handleClose(option) {
    this.props.getSelectedLocation(option)
    this.place = option;
    this.setState({
      placeValue: option.description,
      places: []
    });
  }
  componentDidMount() {
    this.google = window.google || global.google;
    this.initMap();
  }
  render() {
    const {classes, inputProps, inputLabel=null} = this.props;


    return (
          <div>
            {inputLabel}
            <Input
              inputRef={(node)=> this.inputGeo = node}
              id="name-disabled"
              value={this.state.placeValue}
              onChange={this.getSuggestion}
              {...inputProps}
            />
            {(!this.state.places.length) ? null :
              <Paper className={classes.menu}>
                <MenuList>
                  {this.state.places.map((option, i) => {
                    console.debug(i)
                    console.debug(this.state.selected)
                    return [
                      <MenuItem
                        key={i}
                        selected={(this.state.selected !== null && this.state.selected === i)}
                        onClick={() => this.handleClose(option)}
                      >
                        {option.description}
                      </MenuItem>,
                      (this.state.places.length > i) ? <Divider light/> : null
                    ];
                  })}
                </MenuList>
              </Paper>
            }
          </div>
    );
  }
}



InputGeocode.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  getSelectedLocation: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(InputGeocode);