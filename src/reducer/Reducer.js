import { combineReducers } from 'redux'

import ReducerProfile from "./ReducerProfile"
import ReducerCreateRentForm from "./ReducerCreateRentForm"


export const reducer = combineReducers({
  UserProfile: ReducerProfile,
  CreateRentForm: ReducerCreateRentForm
});
