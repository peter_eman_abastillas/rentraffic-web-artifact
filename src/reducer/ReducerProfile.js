const init_profile = {
  isLoaded: false,
  isFetching: false,
  userPool: false,
  image_temp: [],
  id:"",
  username:"",
  email:"",
  last_login:"",
  active:"",
  first_name:"",
  last_name:"",
  phone:"",
  profile_pic:[],
  user_type:"",
  location:"",
  address:"",
  city:"",
  lat:"",
  lng:""
};



export default function reducer(state=init_profile, action) {
  switch (action.type) {
    case "PROFILE_EDIT_FIRST_NAME":
      return {
        ...state,
        ...action
      };
    case "PROFILE_EDIT_LAST_NAME":
      return {
        ...state,
        ...action
      };
    case "PROFILE_EDIT_EMAIL":
      return {
        ...state,
        ...action
      };
    case "FETCH_USER_LOADING":
      return {
        ...state,
        isLoaded: action.isLoaded,
        isFetching: true
      };
    case "FETCH_USER_LOADED":
      let profile_pic=action.profile_pic
      if(typeof action.payload.profile_pic === "string") {
        profile_pic = JSON.parse(action.payload.profile_pic);
      }
      return {
        ...state,
        ...action.payload,
        profile_pic:profile_pic,
        hasSession: true,
        isLoaded: true,
        isFetching: false
      };
    case "FETCH_USER_ERROR":
      return {
        ...state,
        isLoaded: false,
        isFetching: false
      };
    case "FETCH_USER_POOL_NO_SESSION":
      return {
        ...state,
        isLoaded: false,
        isFetching: false
      };
    case "PROFILE_IMAGE_UPLOADING":
      let image_temp = state.image_temp;
      let action_temp_image = action.image_temp;

      image_temp[action.key] = action_temp_image;
      image_temp[action.key].isUploaded = false;

      return {
        ...state,
        image_temp
      };
    case "PROFILE_IMAGE_UPLOADED":
      let images_temp = state.image_temp;
      let image = state.profile_pic || [];
      images_temp[action.key].isUploaded = true;

      image.push(action.image);
      return {
        ...state,
        image,
        profile_pic: image,
      };
    default:
      return {
        ...state,
        ...action
      };
  }
  return state;

}
