import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";

import Home from "./containers/Home";
import Login from "./containers/Authentication/Login";
import ForgotPassword from "./containers/Authentication/ForgotPassword";
import Signup from "./containers/Authentication/Signup";
import CreateRent from "./containers/Rent/CreateRent";
import ViewProfile from "./containers/Profile/ViewProfile";
import EditProfile from "./containers/Profile/EditProfile";
import NewNote from "./containers/NewNote";
import NotFound from "./containers/NotFound";

export default ({ childProps }) => {
    return (<Switch>
        <AppliedRoute path="/" exact component={Home} props={childProps}/>
        <UnauthenticatedRoute path="/login" exact component={Login} props={childProps}/>
        <UnauthenticatedRoute path="/signup" exact component={Signup} props={childProps}/>
        <UnauthenticatedRoute path="/forgot_password" exact component={ForgotPassword} props={childProps}/>
        <AuthenticatedRoute path="/user/view/:user" component={ViewProfile} props={childProps}/>
        <AuthenticatedRoute path="/user/edit" component={EditProfile} props={childProps}/>
        <AuthenticatedRoute path="/rent/create" exact component={CreateRent} props={childProps}/>
        <Route component={NotFound}/>
    </Switch>);
}