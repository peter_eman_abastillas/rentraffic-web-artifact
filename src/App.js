import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { authUser, signOutUser, getCurrentUser, getUserToken } from "./libs/awsLib";
import "./App.css";
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Routes from './Routes';
import {getUser} from "./action/ActionProfile";
import {connect} from "react-redux"
import config from './config'
import Avatar from 'material-ui/Avatar';
import classNames from 'classnames';
import Menu, { MenuItem } from 'material-ui/Menu';
import Grid from 'material-ui/Grid';


const ITEM_HEIGHT = 48;

const styles = {
  root: {
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  appBar: {
    marginBottom: 20
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 40,
    height: 40,
  },
  container: {
    margin:"20px auto 0",
    maxWidth: 1024
  }
};


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true,
      cognito: {},
      token: null,
      anchorEl: null
    };
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  async componentWillMount() {
    try {
      if (await authUser()) {
        this.userHasAuthenticated(true);
      }
    }
    catch (e) {
      alert(e);
    }

    this.setState({isAuthenticating: false});
  }

  userHasAuthenticated = async authenticated => {
    var current_user = getCurrentUser();
    var token = await getUserToken(current_user);
    await this.props.dispatch(getUser(token, current_user.username));
    this.setState({
      isAuthenticated: authenticated,
      cognito: current_user,
      token: token
    });

  }

  async getProfile() {
    await this.props.dispatch(getUser(this.state.token, this.state.cognito.username));

  }

  handleLogout = event => {
    signOutUser();

    this.userHasAuthenticated(false);

    this.props.history.push("/login");
  }
  handleLink = (event, arg) => {
    this.props.history.push(arg);
  }

  render() {
    const {classes, UserProfile} = this.props;
    console.debug(this.props);
    const childProps = {
      config: config,
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,
      cognito: this.state.cognito,
      token: this.state.token
    };

    let profile_pic = this.state.isAuthenticated ? <Avatar
      alt= {UserProfile.first_name + " " + UserProfile.last_name}
      className={classNames(classes.avatar, classes.bigAvatar)}
      onClick={this.handleClick}

    >{UserProfile.first_name.charAt(0).toUpperCase()}{UserProfile.last_name.charAt(0).toUpperCase()}</Avatar> : null;

    if(!!UserProfile.profile_pic) {
      profile_pic = <Avatar
        onClick={this.handleClick}
        alt= {UserProfile.first_name + " " + UserProfile.last_name}
        src={UserProfile.profile_pic[0]}
        className={classNames(classes.avatar, classes.bigAvatar)}
      />
    }


    return (
      <div>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon/>
            </IconButton>
            <Typography onClick={(e) => this.handleLink(e, "/")} style={{cursor: 'pointer'}} type="title"
                        color="inherit" className={classes.flex}>
              <img src="/img/rentraffic.png" height={56}/>
            </Typography>

            <Typography type="title" color="inherit" stlye={{flex: 0}}>
              {this.state.isAuthenticated
                ? [
                  <Button key={0} secondary="true" onClick={(e) => this.handleLink(e, "/rent/create")}>Rent Your
                    Item</Button>,
                ]
                : [
                  <Button key={0} onClick={(e) => this.handleLink(e, "/signup")}>
                    Sign up
                  </Button>,
                  <Button key={1} onClick={(e) => this.handleLink(e, '/login')}>
                    Login
                  </Button>
                ]}
            </Typography>
              {profile_pic}
              {this.state.isAuthenticated
              ?
                <Menu
                  id="long-menu"
                  anchorEl={this.state.anchorEl}
                  open={Boolean(this.state.anchorEl)}
                  onClose={this.handleClose}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: 200,
                    },
                  }}
                >
                    <MenuItem key={0}  onClick={(e) => {
                      this.handleClose();
                    this.handleLink(e, "/user/view/"+UserProfile.id)
                    }}>
                      Your Profile
                    </MenuItem>
                    <MenuItem key={1}  onClick={() => {
                      this.handleClose();
                      this.handleLogout();
                    }}>
                      Logout
                    </MenuItem>

                </Menu>
              : null
              }
          </Toolbar>
        </AppBar>
        <Grid container className={classes.container} spacing={24}>
          <Routes childProps={childProps}/>
        </Grid>
      </div>
    )
  }
}
App = withStyles(styles)(App);
App = connect((store) => {
  return {...store}
})(App);


export default withRouter(App);
